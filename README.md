# Pyrit

**A modular Python-based finite element solver for multiphysics problems**

---

[toc]

---

## Abstract

**Pyrit** is a Finite Element Method Based numerical field simulation software written in Python to solve coupled systems of partial differential equations. Currently, the modular solver covers static and quasistatic electric and magnetic fields, stationary current problems, stationary, and transient heat conduction problems. The different modules can be coupled to analyze multiphysical engineering applications, such as e.g. *foil windings, cable joints, and surge arresters.*
	The researchers working on the project are part of the [Athene Young Investigator research group Quasistatics in Computational Engineering (QuinCE)](https://www.temf.tu-darmstadt.de/emft/forschung_emft/forschungsgruppen_emft/quasistatik_in_computational_engineering.de.jsp) at the [Institute for Accelerator Science and Electromagnetic Fields (TEMF)](https://www.temf.tu-darmstadt.de/emft/home_emft/index.de.jsp) at TU Darmstadt.

## Introduction

Today, the application of electromagnetic field simulation software is essential in science as well as in industry. Many different simulation software tools for electromagnetic field simulation based on the finite element method have been developed over time. 
Commercial software tools are designed to solve universal electromagnetic field problems with standard computational methods. Consequently, they are modifiable only up to a certain point and, typically, no direct interface to the solver exists. 
Thus, the context of research projects on new simulation methods, an in-house solver, which allows for problem-specific modifications and extensions is desired.

Therefore, a finite element solver written in Python for solving static and quasistatic electromagnetic and thermal problems on two- and three-dimensional domains with the name *Pyrit* is being developed. 
For the geometry and mesh generation the open source software Gmsh [\[1\]](#gmsh) and its Python API is utilized.

## Key features

Current features include:

- Nonlinear static and quasistatic electric problems
- Nonlinear static and magnetic problems
- Nonlinear stationary current problems
- Nonlinear stationary and transient heat conduction problems
- Available shape functions:
  - Nodal shape functions on 2D Cartesian, 2D Axisymmetric and 3D meshes
  - Edge shape functions on 2D Axisymmetric meshes
- Enhanced interface to Gmsh
- Straightforward plotting of field solutions and export to LaTeX

## How to access the code

The code is protected by the [GNU AGPLv3 license](https://choosealicense.com/licenses/agpl-3.0/). It is available on request for members of TU Darmstadt, researchers, and engineers working in research and industry. Please send an email to [jonas.bundschuh@tu-darmstadt.de](mailto:jonas.bundschuh@tu-darmstadt.de) for further information.

When you used *Pyrit*, please cite the [paper](https://arxiv.org/abs/2210.11983) [\[2\]](#pyritpaper)

## Contributors

Conceptualization and main contributors:

- Jonas Bundschuh
- Greta Ruppert
- Yvonne Späck-Leigsnering
- Laura D'Angelo

Further contributions came from:

- Jonas Christ
- Daniel Leißner
- Svenja Menzenbach
- Katharina Thyssen
- Leon Blumrich
- Nadine Seif
- Hubert Lögl
- Koray Coscun

## Acknowledgments 

The project is supported by the German Science Foundation (DFG project 436819664) and by the Federal Ministry of Education and Research (BMBF project 05K19RDB). 



## References 

<a name="gmsh"></a> [1] Christophe Geuzaine and Jean-François Remacle, *Gmsh*, https://gmsh.info/

<a name="pyritpaper"></a> [2] Jonas Bundschuh, M. Greta Ruppert, Yvonne Späck-Leigsnering, *Pyrit: A Finite Element Based Field Simulation Software Written in Python*
